package DAO;

import Banco.DBConexao;
import br.com.ifms.model.Contato;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ContatoDAO {

    private final String INSERT = "INSERT INTO CONTATO(nome,telefone,email) VALUES(?,?,?)";
    private final String DELETE = "DELETE FROM CONTATO where id = ?";
    private final String LIST = "SELECT * FROM CONTATO";
    private final String LISTBYID = "SELECT * FROM CONTATO where id = ?";
    private final String LISTBYNOMETELEFONE = "SELECT * FROM CONTATO where nome = ? and telefone= ? ";
    private final String UPDATE = "UPDATE CONTATO SET nome = ?, telefone = ?, email = ? where id = ?";
    private final String CONTAR = "SELECT COUNT(*) as total FROM CONTATO";
        
    public void inserir(Contato contato) {

        if (contato != null) {
            Connection conn = null;

            try {

                conn = DBConexao.getConexao();

                PreparedStatement pstm;

                pstm = conn.prepareStatement(INSERT);
                

                pstm.setString(1, contato.getNome());

                pstm.setString(2, contato.getTelefone());

                pstm.setString(3, contato.getEmail());

                pstm.execute();

                JOptionPane.showMessageDialog(null, "Contato cadastrado com sucesso");

                //  DBConexao.fechaConexao(conn, pstm);
            } catch (Exception e) {

                JOptionPane.showMessageDialog(null, "Erro ao inserir contato no banco de"
                        + "dados " + e.getMessage());

            }

        } else {

            System.out.println("O contato enviado por parâmetro está vazio");

        }

    }

    public void remover(int id) {
        Connection conn = null;
        try {
            conn = DBConexao.getConexao();
            PreparedStatement pstm;
            pstm = conn.prepareStatement(DELETE);

            pstm.setInt(1, id);          
            pstm.execute();
            JOptionPane.showMessageDialog(null, "OK");
            //DBConexao.fechaConexao(conn,pstm);
  

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir contato do banco de dados" + e.getMessage());
        }

    }
    
    
    
    public void atualizar(Contato contato) {
        Connection conn = null;
        try {
            conn = DBConexao.getConexao();
            PreparedStatement pstm;
            pstm = conn.prepareStatement(UPDATE);

            pstm.setString(1, contato.getNome());
            pstm.setString(2, contato.getTelefone());
            pstm.setString(3, contato.getEmail());
            pstm.setInt(4, contato.getId());
            pstm.execute();
            JOptionPane.showMessageDialog(null, "OK");
            //DBConexao.fechaConexao(conn,pstm);
  

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao atualizar contato do banco de dados" + e.getMessage());
        }

    }

    
    public List<Contato> getContatos(){
        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        ArrayList<Contato> contatos = new ArrayList<Contato>();
        try{
            conn = DBConexao.getConexao();
            pstm = conn.prepareStatement(LIST);
            rs = pstm.executeQuery();
            while(rs.next()){
                Contato contato = new Contato();
                
                contato.setId(rs.getInt("id"));
                contato.setNome(rs.getString("nome"));
                contato.setTelefone(rs.getString("telefone"));
                contato.setEmail(rs.getString("email"));
                contatos.add(contato);
            }
            DBConexao.fechaConexao(conn, pstm, rs);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "O erro foi" + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        return contatos;
    }
    
    public Contato getContatoById(int id){
        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        Contato contato = new Contato();
        try{
            conn = DBConexao.getConexao();
            pstm = conn.prepareStatement(LISTBYID);
            pstm.setInt(1, id);
            rs = pstm.executeQuery();
            while(rs.next()){
                contato.setId(rs.getInt("id"));
                contato.setNome(rs.getString("nome"));
                contato.setTelefone(rs.getString("telefone"));
                contato.setEmail(rs.getString("email"));
            }
            //DBConexao.fechaConexao(conn, pstm, rs);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "O erro foi" + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        return contato;
    }
    
    
    public Integer getQuantidade(){
        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rs;
        
        int valor = 0;
        
        try{
            conn = DBConexao.getConexao();
            pstm = conn.prepareStatement(CONTAR);
            rs = pstm.executeQuery();
            while(rs.next()){
                valor = rs.getInt("total");
            }
            //DBConexao.fechaConexao(conn, pstm, rs);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "O erro foi " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        return valor;
    }
    
    
    
    
    
    public static void main(String[] args) {
        Contato novo = new Contato();
        ContatoDAO cdao = new ContatoDAO();

        novo.setNome("soumaya");
        novo.setTelefone("222");
        novo.setEmail("soumaya@gmail.com");

        //cdao.inserir(novo);
        //cdao.remover(2);
        
        
        
         System.out.println(cdao.getContatoNomeTel(novo));
        
        //novo.setNome("otavio");
        
        //cdao.atualizar(novo);
        //List<Contato> contatos = cdao.getContatos();
        
        /*for(Contato i : contatos)
        {
            System.out.println(i.getNome());
        }*/
        
        //System.out.println(cdao.getContatosById(1).getNome());
        
        //int total = cdao.getQuantidade();
       // System.out.println(total);
       
       
    }



    public Contato getContatoNomeTel(Contato c) {
        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        Contato contato = new Contato();
        try{
            conn = DBConexao.getConexao();
            pstm = conn.prepareStatement(LISTBYNOMETELEFONE);
            pstm.setString(1, c.getNome());
            pstm.setString(2, c.getTelefone());
            rs = pstm.executeQuery();
            while(rs.next()){
                contato.setId(rs.getInt("id"));
                contato.setNome(rs.getString("nome"));
                contato.setTelefone(rs.getString("telefone"));
                contato.setEmail(rs.getString("email"));
            }
            //DBConexao.fechaConexao(conn, pstm, rs);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "O erro foi" + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        return contato;
    }
}
