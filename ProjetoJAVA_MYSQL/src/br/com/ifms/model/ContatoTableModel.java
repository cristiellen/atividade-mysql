
package br.com.ifms.model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;


public class ContatoTableModel extends AbstractTableModel {
    
    private static final int COL_ID =0;
    private static final int COL_NOME=1;
    private static final int COL_TELEFONE=2;
    private static final int COL_EMAIL=3;
    
    List<Contato>linhas;
    private final String[]colunas= new String[]{"id,nome,telefone,email"};

    public ContatoTableModel(List<Contato>contatos)
    {
        this.linhas= new ArrayList<>(contatos);
    }
    @Override
    public int getRowCount() {
       return linhas.size();
    }

    @Override
    public int getColumnCount() {
       return colunas.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
      return colunas[columnIndex];
    }
    
    public Class getColumnClass(int columnIndex)
    {
        if(columnIndex== COL_ID)
        {
            return Integer.class;
        }
        return String.class;
    }
    
}
