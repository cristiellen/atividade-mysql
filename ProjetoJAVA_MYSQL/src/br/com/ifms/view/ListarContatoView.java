
package br.com.ifms.view;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

public class ListarContatoView {
    
    private JFrame janela;
    private JPanel painelFundo;
    private JPanel painelBotoes;
    private JTable tabela;
    private JScrollPane barraRolagem;
    private JButton btInserir;
    private JButton btExcluir;
    private JButton btEditar;	
    
    
    public void criaJanela(){
    
        janela = new JFrame("CONTATOS"); //Nome da janela LP2
        janela.setBounds(200, 30, 700, 400); // setBounds(x,y,tamx,tamy)
        janela.getContentPane().setLayout(new BorderLayout());
        
        btInserir = new JButton("Inserir");
        btEditar = new JButton("Editar");
        btExcluir = new JButton("Excluir");
        
        painelFundo = new JPanel();
        painelFundo.setLayout(new BorderLayout());
        painelBotoes = new JPanel();
        
        painelBotoes.add(btInserir);
        painelBotoes.add(btEditar);
        painelBotoes.add(btExcluir);
        
        tabela = new JTable();
        barraRolagem = new JScrollPane(tabela);
        painelFundo.add(BorderLayout.CENTER, barraRolagem);
        
        janela.getContentPane().add(painelFundo, BorderLayout.CENTER);
        janela.getContentPane().add(painelBotoes, BorderLayout.SOUTH);
        
        janela.setVisible(true);
        janela.setResizable(false);
    }
    
    public void listarContato(){}
    
    private void criaJTable(){}
    
    public static void pesquisar(DefaultTableModel modelo){}
    
    
    
    public static void main(String[] args) {
        
        ListarContatoView lc = new ListarContatoView();
        
        lc.criaJanela();
        
    }

}
